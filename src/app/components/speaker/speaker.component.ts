import { Component, OnInit, Input } from '@angular/core';
import { SpeakerService } from '../../services/api/speaker.service';

@Component({
  selector: 'app-speaker',
  templateUrl: './speaker.component.html',
  styleUrls: ['./speaker.component.scss']
})
export class SpeakerComponent implements OnInit {
  @Input('speaker') speaker: any;

  constructor(
    public speakerService: SpeakerService,
  ) { }

  ngOnInit(): void {
  }

  /**
   * 발표자 LIVE / VOD 리스트 조회
   */
  getDetail = (speaker) => {
    this.speakerService.findOne(speaker.id).subscribe(res => {
      this.speaker = res;
    });
  }

}
