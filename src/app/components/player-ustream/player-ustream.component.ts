import { Component, OnInit, AfterViewInit, ChangeDetectorRef, HostListener, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { _ParseAST } from '@angular/compiler';
// import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
import '@devmobiliza/videojs-vimeo/dist/videojs-vimeo.esm';
import { MemberService } from 'src/app/services/api/member.service';
import * as _ from 'lodash';
import { DomSanitizer } from '@angular/platform-browser';

declare let UstreamEmbed: any;

/**
 * TODO
 * Ustream 라이브시 이 컴포넌트를 사용한다.
 * 삼성서울병원 전용 air에서 개발 되어서. Room이 하나밖에 없었기 때문에 live url이 하드코딩 되어있다.
 * 각 룸에 liveUrl을 등록 해서 url 추적하는 방식으로 개발 필요하다.
 */
@Component({
  selector: 'app-player-ustream',
  templateUrl: './player-ustream.component.html',
  styleUrls: ['./player-ustream.component.scss']
})
export class PlayerUstreamComponent implements OnInit, AfterViewInit {
  @Input('content') content: any; // 전달받은 콘텐츠 정보

  public user: any;
  public viewer: any;
  public liveUrl;

  // TODO 임시데이타
  constructor(
    public fb: FormBuilder,
    private cdr: ChangeDetectorRef,
    private memberService: MemberService,
    private sanitizer: DomSanitizer
  ) {
  }

  ngOnInit(): void {
    this.liveUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.content.contentUrl);
    this.user = JSON.parse(sessionStorage.getItem('cfair'));
  }

  ngAfterViewInit(): void {
    this.cdr.detectChanges();
    this.setUstreamViewer();
  }

  setUstreamViewer(): void {
    if (UstreamEmbed) {
      this.viewer = UstreamEmbed('UstreamIframe');
      // document.getElementById('UstreamIframe').style.minHeight = '-webkit-fill-available';

      this.viewer.addListener('live', (event, data) => {
        console.log(event + ' = ' + data);
      });

      this.viewer.addListener('finished', (event, data) => {
        console.log(event + ' = ' + data);
        this.viewer.getProperty('progress', function (progress) {
          console.log('progress = ' + progress);
        });
      });
      this.viewer.addListener('offline', (event, data) => {
        console.log(event + ' = ' + data);
        this.viewer.getProperty('progress', function (progress) {
          console.log('progress = ' + progress);
        });
      });
      this.viewer.addListener('playing', (event, data) => {
        console.log(event + ' = ' + data);
      });
    } else {
      console.log('no UstreamEmbed');
    }
  }
}
