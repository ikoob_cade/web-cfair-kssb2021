import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private serverUrl = '/events/:eventId';

  constructor(
    private http: HttpClient,
  ) { }

  login(body: any): Observable<any> {
    return this.http.post(this.serverUrl + '/login', body);
  }

  logout(): Observable<any> {
    return this.http.post(this.serverUrl + '/logout', null);
  }


}
