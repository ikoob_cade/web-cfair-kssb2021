import * as _ from 'lodash';
import { Component, OnInit, HostListener } from '@angular/core';
import { SpeakerService } from '../../services/api/speaker.service';

@Component({
  selector: 'app-speakers',
  templateUrl: './speakers.component.html',
  styleUrls: ['./speakers.component.scss']
})
export class SpeakersComponent implements OnInit {

  public speakers: Array<any> = [];
  public mobile: boolean = false;

  constructor(
    private speakerService: SpeakerService,
  ) { }

  ngOnInit(): void {
    this.mobile = window.innerWidth < 768;
    this.loadSpeakers();
  }


  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.mobile = window.innerWidth < 768;
  }


  // 발표자 리스트를 조회한다.
  loadSpeakers = () => {
    this.speakerService.find(false).subscribe(res => {
      const speakers = res;
      this.speakers = _.sortBy(speakers, 'seq');
    });
  }

}
