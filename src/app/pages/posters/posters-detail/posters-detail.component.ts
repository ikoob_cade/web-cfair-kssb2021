import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PosterService } from 'src/app/services/api/poster.service';

@Component({
  selector: 'app-posters-detail',
  templateUrl: './posters-detail.component.html',
  styleUrls: ['./posters-detail.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PostersDetailComponent implements OnInit {
  @ViewChild('pdfViewer') pdfViewer: any;

  posterId: string;
  selectedPoster: any = [];

  slide = null;
  slideCurrentPage = 1;
  slideTotalPage;

  constructor(
    public route: ActivatedRoute,
    private posterService: PosterService,
  ) {

    this.posterId = route.snapshot.params['posterId']
  }

  ngOnInit(): void {
    this.getPosterDetail();
  }

  getPosterDetail(): void {
    this.posterService.findOne(this.posterId).subscribe(res => {
      this.selectedPoster = res;
      console.log(this.selectedPoster.contents.contentUrl);
      if (this.selectedPoster.contents && this.selectedPoster.contents.contentUrl.includes('.pdf')) {
        this.slide = {
          url: this.selectedPoster.contents.contentUrl,
          credential: true,
        };
      }

    });
  }

  loadedSlide(event) {
    event.getPage(1);
    this.slideTotalPage = event._pdfInfo.numPages;
  }

  setSlide(amount: number) {
    if (this.slideCurrentPage <= this.slideTotalPage) {
      this.slideCurrentPage += amount;
    }
  }

}
