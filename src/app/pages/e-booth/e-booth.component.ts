import * as _ from 'lodash';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { BoothService } from '../../services/api/booth.service';

declare var $: any;
@Component({
  selector: 'app-e-booth',
  templateUrl: './e-booth.component.html',
  styleUrls: ['./e-booth.component.scss']
})
export class EBoothComponent implements OnInit {
  @ViewChild('winAlertBtn') winAlertBtn: ElementRef;

  public sponsors: Array<any> = []; // 스폰서 목록
  private categories: Array<any> = [];
  public categories1: Array<any> = []; // 카테고리[부스] 목록
  public categories2: Array<any> = []; // 카테고리[부스] 목록

  public selectedBooth = null;

  public attachments = [];
  public random = 0;

  public isLoaded = false;

  private stampTourAccess = false;

  constructor(
    private boothService: BoothService,
  ) { }

  ngOnInit(): void {
    // this.attachments = this.setAttachments();
    $('#e-boothModal').on('hidden.bs.modal', () => {
      document.getElementById('boothModalDesc').innerHTML = null;
    });

    this.loadBooths();
    // this.loadSponsors();
  }

  /** 모달에 첨부파일 셋팅 */
  setAttachments(): void {
    if (this.selectedBooth) {
      this.attachments = _.map(this.selectedBooth.contents, (content) => {
        if (content.contentType === 'slide') {
          return content;
        }
      });
    }
  }

  /**
   * 부스 목록 조회
   *
   * atweek2020 작업중 작성
   * 카드형식의 업체도 스탬프투어가 적용되어야할경우 부스관리에 등록 후 배열을 직접 잘라서(slice) html에 배치.
   */
  loadBooths = () => {
    this.boothService.find().subscribe(res => {
      // const booths = res.slice(0, 6);
      const booths = res;
      if (res.length < 1) return;
      this.categories =
        _.chain(booths)
          .groupBy(booth => {
            return booth.category ? JSON.stringify(booth.category) : '{}';
          })
          .map((booth, category) => {
            category = JSON.parse(category);
            category.booths = booth;
            return category;
          }).sortBy(category => {
            return category.seq;
          })
          .value();

      // const sponsors = res.slice(6, 12);

      // this.sponsors = _.chain(sponsors)
      //   .groupBy(booth => {
      //     return booth.category ? JSON.stringify(booth.category) : '{}';
      //   })
      //   .map((booth, category) => {
      //     category = JSON.parse(category);
      //     category.booths = booth;
      //     return category;
      //   }).sortBy(category => {
      //     return category.seq;
      //   })
      //   .value();

      this.categories = _.sortBy(this.categories, 'seq');
      _.forEach(this.categories, category => {
        category.categoryName = category.categoryName.toLowerCase();
      });

      this.categories1 = this.categories;
      // this.categories2 = this.categories.slice(4, 6);

      this.isLoaded = true;
    });

  }

  /**
   * 스폰서 목록 조회
   */
  // loadSponsors = () => {
  //   this.sponsorService.find().subscribe(res => {
  //     this.sponsors = res;
  //   });
  // }

  // 모달의 부스내용을 선택한 아이템 데이터로 변경
  setDesc(): void {
    if (this.selectedBooth.description) {
      document.getElementById('boothModalDesc').innerHTML = this.selectedBooth.description;
    }
  }

  // 부스 선택
  getDetail = (selectedBooth) => {
    const user = JSON.parse(sessionStorage.getItem('cfair'));
    let memberId = null;
    if (this.stampTourAccess) {
      if (user && user.isLog) {
        memberId = user.id;
      }
    }

    this.boothService
      .findOne(selectedBooth.id, memberId)
      .subscribe(res => {
        this.selectedBooth = res;
        this.random = Math.floor(Math.random() * 10);
        if (res.tourSuccess) {
          this.winAlertBtn.nativeElement.click();
        }

        this.setAttachments();
        this.setDesc();
      });
  }

}
